import string


class Polibi(object):
    def __init__(self, lang='ru', step=6):
        super(Polibi, self).__init__()
        self.step = step
        self.lang = lang
        self.letters = 'АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ' if self.lang == 'ru' else string.ascii_uppercase
        self.letters += ',.- '
        self.matrix = self.__generate_matrix()
        self.indexes = self.letters[:self.step]
        self.source_text = ''
        self.encode_text = ''
        self.decode_text = ''

    def __generate_matrix(self):
        matrix = [self.letters[index: index + self.step] for index in range(0, len(self.letters), self.step)]
        for index, line in enumerate(matrix):
            if len(line) < self.step:
                matrix[index] = line + '-' * (self.step - len(line))
        return matrix

    def encode(self, txt=''):
        self.encode_text = ''
        self.source_text = txt.upper()
        for char in self.source_text:
            for index, line in enumerate(self.matrix):
                if char in line:
                    self.encode_text += self.indexes[index]
                    self.encode_text += self.indexes[line.index(char)]
        return self.encode_text

    def decode(self, txt=''):
        self.decode_text = ''
        self.encode_text = txt if txt else self.encode_text
        blocks = [self.encode_text[index: index + 2] for index in range(0, len(self.encode_text), 2)]
        for block in blocks:
            try:
                i, j = self.letters.index(block[0]), self.letters.index(block[1])
                self.decode_text += self.matrix[i][j]
            except (ValueError, UnboundLocalError):
                print('Не удалось расшифровать строку')
                break
        return self.decode_text


if __name__ == '__main__':
    while True:
        text = input()
        crypto = Polibi()
        print('Encode: {}\nDecode: {}'.format(crypto.encode(txt=text), crypto.decode()))
